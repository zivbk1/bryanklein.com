+++
date = "2018-04-19T21:14:20-07:00"
title = "Board Gamer, Role Player and GM"
type = "page"
+++

## I like running games and playing them!

I have a nice collection of games some with boards and many without. I recently took the "[What Kind of Dungeons and Dragons Character Would You Be?](http://www.easydamus.com/character.html)" survey, the results were interesting and fun.
<!--more-->

**True Neutral Halfling Wizard/Sorcerer (4th/3rd Level)**

## Ability Scores:
**Strength:** 13  
**Dexterity:** 16  
**Constitution:** 12  
**Intelligence:** 16  
**Wisdom:** 14  
**Charisma:** 15

## Alignment:
**True Neutral** A true neutral character does what seems to be a good idea. He doesn't feel strongly one way or the other when it comes to good vs. evil or law vs. chaos. Most true neutral characters exhibit a lack of conviction or bias rather than a commitment to neutrality. Such a character thinks of good as better than evil after all, he would rather have good neighbors and rulers than evil ones. Still, he's not personally committed to upholding good in any abstract or universal way. Some true neutral characters, on the other hand, commit themselves philosophically to neutrality. They see good, evil, law, and chaos as prejudices and dangerous extremes. They advocate the middle way of neutrality as the best, most balanced road in the long run. True neutral is the best alignment you can be because it means you act naturally, without prejudice or compulsion. However, true neutral can be a dangerous alignment when it represents apathy, indifference, and a lack of conviction.

## Race:
**Halflings** are clever, capable and resourceful survivors. They are notoriously curious and show a daring that many larger people can't match. They can be lured by wealth but tend to spend rather than hoard. They prefer practical clothing and would rather wear a comfortable shirt than jewelry. Halflings stand about 3 feet tall and commonly live to see 150.

## Primary Class:
**Wizards** are arcane spellcasters who depend on intensive study to create their magic. To wizards, magic is not a talent but a difficult, rewarding art. When they are prepared for battle, wizards can use their spells to devastating effect. When caught by surprise, they are vulnerable. The wizard's strength is her spells, everything else is secondary. She learns new spells as she experiments and grows in experience, and she can also learn them from other wizards. In addition, over time a wizard learns to manipulate her spells so they go farther, work better, or are improved in some other way. A wizard can call a familiar- a small, magical, animal companion that serves her. With a high Intelligence, wizards are capable of casting very high levels of spells.

## Secondary Class:
**Sorcerers** are arcane spellcasters who manipulate magic energy with imagination and talent rather than studious discipline. They have no books, no mentors, no theories just raw power that they direct at will. Sorcerers know fewer spells than wizards do and acquire them more slowly, but they can cast individual spells more often and have no need to prepare their incantations ahead of time. Also unlike wizards, sorcerers cannot specialize in a school of magic. Since sorcerers gain their powers without undergoing the years of rigorous study that wizards go through, they have more time to learn fighting skills and are proficient with simple weapons. Charisma is very important for sorcerers; the higher their value in this ability, the higher the spell level they can cast.

---
## Detailed Results

**Alignment:**
```
Lawful Good ----- XXXXXXXXXXXXXXXXXX (18)
Neutral Good ---- XXXXXXXXXXXXXXXXXXXXX (21)
Chaotic Good ---- XXXXXXXXXXXXXXXXXX (18)
Lawful Neutral -- XXXXXXXXXXXXXXXXXXX (19)
True Neutral ---- XXXXXXXXXXXXXXXXXXXXXX (22)
Chaotic Neutral - XXXXXXXXXXXXXXXXXXX (19)
Lawful Evil ----- XXXXXXXXX (9)
Neutral Evil ---- XXXXXXXXXXXX (12)
Chaotic Evil ---- XXXXXXXXX (9)
```
**Law & Chaos:**  
```
Law ----- XXXXXXX (7)
Neutral - XXXXXXXXXX (10)
Chaos --- XXXXXXX (7)
```

**Good & Evil:**  
```
Good ---- XXXXXXXXXXX (11)
Neutral - XXXXXXXXXXXX (12)
Evil ---- XX (2)
```

**Race:**  
```
Human ---- XXXXXXXXXXXXX (13)
Dwarf ---- XX (2)
Elf ------ XXXXXXXXXX (10)
Gnome ---- XXXXXXXX (8)
Halfling - XXXXXXXXXXXXXXXX (16)
Half-Elf - XXXXXXXXX (9)
Half-Orc - XXXX (4)
```

**Class:**  
```
Barbarian - XXXXXXXX (8)
Bard ------ XXXXXXXXXXXX (12)
Cleric ---- XXXX (4)
Druid ----- XXXXXXXX (8)
Fighter --- XXXXXXXXXXXX (12)
Monk ------ XXXXXXXXXXXX (12)
Paladin --- XXXXXXXXXXXXXXXX (16)
Ranger ---- XXXXXXXXXXXXXXXX (16)
Rogue ----- XX (2)
Sorcerer -- XXXXXXXXXXXXXXXXXX (18)
Warlock --- XXXXXX (6)
Wizard ---- XXXXXXXXXXXXXXXXXX (18)
```

That's pretty accurate! Now it's your turn to find out [What Kind of Dungeons and Dragons Character Would You Be?](http://www.easydamus.com/character.html)