+++
date = "2016-11-07T00:00:00Z"
title = "About Bryan Klein"
type = "page"

+++
![](/forestryio/images/4of6.JPG)

I wear many different hats:

* Husband
* Father
* Brother
* Friend
* Software Product Manager
* [Gamer](/gaming)

I work for [Thunderhead Engineering](http://www.thunderheadeng.com), located in Manhattan, Kansas. I am very happy to know about such a nice place to live and work in the middle of our nation. I lived in Manhattan for 2 years and have many good friends and co-workers there.

In 2012 I moved to Anacortes, WA which was one of the best things that have happened to me in my life. I work remotely from my home office and while it can get a bit lonely at times, I deeply appreciate the opportunity to do so.  I have lived much of my adult life in other states and far from 'home' where my mother, father and brother live. It is good to be at home somewhere close to 'home' again.