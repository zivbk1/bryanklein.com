+++
author = "Bryan Klein"
categories = ["Professional"]
date = "2010-09-13T17:46:02-08:00"
description = "A quick way in Excel to filter multiple columns by different logical operators with an OR Boolean operator."
featured = ""
featuredalt = ""
featuredpath = ""
linktitle = "Multi-column Filter Excel Function"
title = "Multi-column Filter Excel Function"
aliases = ["/2010/09/multi-column-filter-excel-function/"]
+++

So today I needed to filter a fairly giant csv file down to only the rows that contained certain values in either one or the other column.

I wanted only the rows where “PL” was in the country column, OR the email address column had a value that ended with “.pl”.

So I made this little function in the last cell of each row :

`=OR(COUNTIF(K2,"PL"),COUNTIF(L2,"*.pl"))`

Then I filtered the new column that contained either TRUE or FALSE, by all the rows that were TRUE.

And there you have it, a quick way to filter multiple columns by different logical operators with an OR Boolean operator.

Of course this could easily be extended to make some very complex logical comparisons, but I will leave that up to you dear reader.