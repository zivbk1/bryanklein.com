+++
author = "Bryan Klein"
categories = ["Professional"]
date = "2017-06-21T00:39:25-07:00"
description = "Tips for how to setup Hugo and Madoko inside of an Alpine Linux based Docker Image for GitLab CI/CD."
title = "Hugo and Madoko in a Custom Docker Image for GitLab CI/CD"

+++

I migrated to **[Hugo from Wordpress]({{< ref "hugo-site-on-gitlab.md" >}})**, and in that process I have been trying to find ways to optimize the editing, building and hosting systems that I use for this website.

<!--more-->

I have added three new improvements to the system.

1. Inclusion of [Hugo](https://gohugo.io/) binaries in the source repository.
2. [Madoko](https://www.madoko.net/) Document Processing into HTML files.
3. Custom Docker Image for the [GitLab CI/CD](https://about.gitlab.com/features/gitlab-ci-cd/) runner to reduce the time it takes to build everything after a commit to the project.

## Inclusion of Hugo binaries in the source repository

I decided that instead of wasting time downloading the Hugo binaries each time I built my site in the GitLab runner, that I would include the small binary files for Linux and Windows in my **[GitLab Source Repository](https://gitlab.com/zivbk1/bryanklein.com/tree/master/bin)**.  This makes it easy to control the version of Hugo that I use for my site and script the execution within the build container.

## Madoko Document Processing to HTML files

I really like [Madoko](https://www.madoko.net/) for a single source document format, the [Madoko Node.js build package](https://www.npmjs.com/package/madoko), and the [web based editor](https://www.madoko.net/editor.html) provided by the Madoko Project.  The editor can be used online, or can be setup to run locally and allow for editing files on your filesystem by using the [madoko-local Node.js package](https://www.npmjs.com/package/madoko-local).

As a single source format, the Madoko Markdown based file can be rendered to different output targets, including HTML and PDF.  See the [Madoko Reference](http://madoko.org/reference.html) page for more information. Please see this example for **[HTML output](https://bryanklein.com/html_docs/test.html)** from a **[Madoko (.mdk) Source File](https://gitlab.com/zivbk1/bryanklein.com/blob/master/docs/test.mdk)**.

To make this work, I first run Madoko on the .mdk files in the 'docs' directory in my repository.  
````
for filename in docs/*.mdk; do madoko -v --odir=static/html_docs $filename; done
````
The HTML output from Madoko is placed in the 'static/html_docs' directory so that Hugo will build them into the final site.

My next task is to also setup [texlive](https://www.tug.org/texlive/) within the Docker Container and build PDF versions from the same .mdk files.  Something to look forward to.

## Custom Docker Image for the GitLab CI/CD Runner

Finally, I noticed that when the GitLab CI/CD system ran, it always required extra time to download and install the requisite packages and programs to run Hugo and Madoko.  GitLab has a [Container Registry feature](https://gitlab.com/help/user/project/container_registry) for projects on their site. I decided to take advantage of this and create a [Docker Image](https://docs.docker.com/engine/reference/commandline/images/) that I could use to build my site with.  This is a work in progress, and I plan to add more pre and post processing features to the system.  For now it is working great and has cut the build times in half.

As you can see in the **[Dockerfile](https://gitlab.com/zivbk1/bryanklein.com/blob/master/Dockerfile)**, I use an [Alpine Linux](https://hub.docker.com/r/gliderlabs/alpine/) base image. I then establish the working directory.  Update Alpine, install nodejs and git, install the Madoko npm package, then clean up the leftovers.

Now, in less than 30 seconds, the whole thing spins up, builds the docs, builds the Hugo site and hosts it on my custom domain through [CloudFlare](https://www.cloudflare.com/).  That is now about half the time that it took to setup and build the site before the custom image was used.  I expect that as I add in more pre-processing capabilites into the custom docker image, the time savings will continue to increase compared to just using the GitLab Runner images and adding in the necessary features.

## Yet ToDo

I plan on adding in some asset processing features (based on Gulp and/or Bash Scripts) to my Docker Image to optimize my site performance.  I am considering using a service like [Cloudinary](http://cloudinary.com/) to manage my images and will need to come up with a good approach for uploading and inserting them into my content as I edit.  I am really trying to make the content creation process as easy and feature rich as possible for authors.

Please comment here or find me online through links in the sidebar if you have any questions.  There is always more to learn and do.  I am working toward a stable system that I can eventually roll out for other projects.