+++
author = "Bryan Klein"
categories = ["Professional"]
date = "2018-03-28T17:24:55-07:00"
description = "Changes to simplify my site builds and delivery."
title = "Replacing GitLab CI/CD, Docker Image and Gitlab Pages Hosted on Cloudflare, with Netlify"
+++

To simplify and improve the process of building this website, I had to make a few changes.

My previous build and delivery process (written about on this site) was based on GitLab CI/CD using a Custom Docker Image and creating GitLab Pages that were hosted on Cloudflare CDN.
My new setup just uses GitLab for the code repository and Netlify for everything else.

Using Netlify is fairly common for static sites, but the thing that made mine unique and required some of the complexity of the previous setup, is that I wanted to use [Madoko](https://www.madoko.net/) to build some documentation files and include the output of that process into the final website.

In the previous setup, I used a custom Alpine Linux based Docker image and installed into that the Madoko npm package.  This way the image was cached and would not need to download madoko and be rebuilt each time I ran the CI/CD scripts.  Then I could call the madoko command as part of the build script and it would be there ready to go when I needed it.

With a bit more understanding about [how the Netlify build system works](https://www.netlify.com/docs/continuous-deployment/?_ga=2.205983760.221237313.1522257825-1790162624.1518497049#dependencies), I realized that by creating a [package.json](https://gitlab.com/zivbk1/bryanklein.com/blob/master/package.json) file with the madoko dependency in it, Madoko would be included in the build image that Netlify would cache for me and would be ready when I called it in the build command.

This leads to the next bit to sort out, how to call Madoko first and then Hugo to build the site.  Well it turns out, it's not that hard at all, the following is my build command in Netlify.

```bash
for filename in docs/*.mdk; do madoko -v --odir=static/html_docs $filename; done && hugo
```

This iterates through the docs source files and builds them to the html_docs folder in the hugo site static directory.  After that hugo is run.  Easy Peasy!

I did specify in the Netlify build environment variables, the HUGO_VERSION that I wanted to use.  You can read more about this capability on the [Netlify Blog post about it](https://www.netlify.com/blog/2017/04/11/netlify-plus-hugo-0.20-and-beyond/).

I removed the GitLab CI config file, the Dockerfile, the bin directory that I was putting my Hugo binaries in, and only added a new package.json file.

Now I commit the code to the GitLab repository, triggering the build on Netlify which also hosts the site.  The free SSL through Let's Encrypt that comes along for the ride with Netlify saved me $5 a month.

I saved money, time and reduced complexity. :)