+++
title = "Hugo & Asciidoctor, edited with VS Code, built with GitLab CI/CD and hosted on Firebase."
author = "Bryan Klein"
categories = ["Professional"]
date = 2019-01-15T09:32:26-08:00
description = "Showing how Hugo and Asciidoc files can live together, and the tools to make it happen."
linktitle = ""
featured = ""
featuredpath = ""
featuredalt = ""
+++

Why on Earth would you ever do something like this?  Well, because Hugo is a fantastic static site generator that supports Asciidoc format content files with an external helper, and Asciidoc is an excellent plain text documentation format for content that requires a bit more power than Markdown alone can provide. This post goes over what I setup to make this happen.

## Overview

Of course you can roll your own to get the job done, but here is what I used in my solution.

* Docker (container for local development environment and build pipeline)
* Simple Bash Script (to generate PDF and HTML files to augment Hugo content)
* Hugo (static site generator)
* Asciidoctor (when you need more than Markdown can provide)
* VS Code (editor with live preview)
* GitLab (repo & CI/CD)
* Firebase (site hosting)

Here is the [GitLab project repository](https://gitlab.com/bwklein/hugo-with-asciidoc-content) and here is the [Firebase website](https://tecisitedemo.firebaseapp.com/) that comes out of the GitLab CI/CD process. 

## Docker

Docker is not a hard requirement for this setup, but it sure makes life easier. I decided that using a Docker image to create the same environment on my Windows 10 PC that I would be running for my build environment would be ideal. If it works on my local instance, it should work in my CI/CD setup. I used a couple of previous Docker Images for inspiration, special thanks to the [asciidoctor/docker-asciidoctor](https://github.com/asciidoctor/docker-asciidoctor) project for asciidoctor related stuff and the [dettmering/hugo-build](https://github.com/dettmering/hugo-build) project for hugo-extended related stuff.  Together, I formed a more perfect union and found a nice setup that works for me.  Here is my [Dockerfile](https://gitlab.com/bwklein/hugo-with-asciidoc-content/blob/master/Dockerfile) for this demo project.

## Bash Script

I have a simple [Bash script](https://gitlab.com/bwklein/hugo-with-asciidoc-content/blob/master/scripts/build-docs.sh) that runs before Hugo and loops over a 'docs' directory and my Hugo 'content' directory to build HTML and PDF output for 'docs' and only builds PDF versions of my 'content' Asciidoc files.  In this use case, 'docs' is for more complex publications (technical manual, user guide, etc.) and the 'content' is where site pages and blog posts will show up. For the Asciidoc files found in there, I build a PDF version of the post and place it in the same directory as a '[Resource](https://gohugo.io/content-management/page-resources/)' for the post.

## Hugo

[Hugo](https://gohugo.io/) is just awesome, I've blogged about it before, so I won't sing praises again here. I do want to mention that I used Hugo instead of [Antora](https://antora.org/) to build the site because my plan is to do a lot more than just documentation and I wanted the ability to switch between Markdown and Asciidoc for content as needed.  Hugo has '[External Helpers](https://gohugo.io/content-management/formats/#additional-formats-through-external-helpers)' which provides a way to trigger content builds in Asciidoctor when .adoc files are found in the content directory.

## Asciidoctor

[Asciidoctor](https://asciidoctor.org/) is a fantastic tool for generating multiple output formats from plain text source files. If you want to know more about it, there is a nice [overview doc](https://asciidoctor.org/docs/what-is-asciidoc/) for what it is, why it is good and a bit better than Markdown.  In my case, I am using a [Bash Script](https://gitlab.com/bwklein/hugo-with-asciidoc-content/blob/master/scripts/build-docs.sh) and Hugo's External Helper to trigger the Ruby based CLI to build my output files.  In this case, I am limiting this to HTML and PDF outputs, but there are [more options available](https://asciidoctor.org/docs/#supplemental-converters), including a newer Open Document Format plugin [oncletom/asciidoctor-converter-opendocument](https://github.com/oncletom/asciidoctor-converter-opendocument) that I just found out about.

## VS Code

I typically work in [VS Code](https://code.visualstudio.com/), which already has some great extensions to make your life easier when working with Hugo, Asciidoc and Markdown.

* Hugo - [Hugo Language and Syntax Support](https://marketplace.visualstudio.com/items?itemName=budparr.language-hugo-vscode), [Hugo Helper](https://marketplace.visualstudio.com/items?itemName=rusnasonov.vscode-hugo)
* Asciidoctor - [AsciiDoc](https://marketplace.visualstudio.com/items?itemName=joaompinto.asciidoctor-vscode)
* Markdown - [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one), [markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
* Docker - [Docker](https://marketplace.visualstudio.com/items?itemName=PeterJausovec.vscode-docker)
* General Utilities - [Paste Image](https://marketplace.visualstudio.com/items?itemName=mushan.vscode-paste-image)
  
Note: for the Paste Image extension, I modified the settings to place the images from clipboard into an 'images' folder in the same directory as the adoc/md file. I set the 'Base Path' to `${currentFileDir}/images` and the 'Path' to `${currentFileDir}/images`.

## GitLab

Not only do I host the [repository](https://gitlab.com/bwklein/hugo-with-asciidoc-content) with GitLab, but it has a really nice CI/CD system that I am using to build the docs and site and push to Firebase. Here is the [GitLab CI/CD Configuration File](https://gitlab.com/bwklein/hugo-with-asciidoc-content/blob/master/.gitlab-ci.yml) for this setup.

Note: I am using my own [bwklein/hugo-asciidoctor](https://hub.docker.com/r/bwklein/hugo-asciidoctor) image for the build environment, feel free to use my Dockerfile to create your own image and use it instead of mine.

## Firebase

I know that Netlify is awesome, but hear me out.  I decided to try Firebase for hosting for a couple of reasons.

1. Fast CDN from Google, I was influenced to try it from this post about [which static site hosting platform is fastest](https://www.savjee.be/2017/10/Static-website-hosting-who-is-fastest/) and this [post from another perspective](https://yrezgui.com/blog/2018/04/where-should-i-host-my-static-website).
2. Thinking of a way to implement content protection with the Auth system (back burner). I don't want to pay a bunch of money to password protect some content on a static site, so I am looking for and testing alternatives.
3. Netlify doesn't yet have a [build image that works with Hugo extended](https://github.com/netlify/build-image/issues/182). (I have tested a dev version that did and I hope it is released soon.)

## What's Next?

This was a first pass proof of concept for a new site based on Hugo and updated documentation for our products that will be converted from existing MS Word files to Asciidoc. I am always happy to receive any feedback or suggestions you might have.