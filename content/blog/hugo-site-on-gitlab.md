+++
author = "Bryan Klein"
categories = ["Professional"]
date = "2016-12-21T16:19:32-08:00"
description = "A short overview of what I did to host this Hugo based website on GitLab."
linktitle = "How I setup and host this blog."
title = "Hosting Hugo on GitLab"
+++

I recently **[decided to switch from Wordpress to a Static Site]({{< ref "transition-from-wordpress.md" >}})**. This post is a review of what I finally ended up with for the most simple solution for my site. I had a few guiding principles as I worked through the process of setting this up.

*   Free or Cheap static site hosting
*   As few moving parts and systems as possible
*   Easy to create or update content from desktop or mobile
*   Supports SSL (HTTPS) for my domain

<!--more-->

I tested many different tools and configurations before I ended up where I am now. Here are a few of the sites/services/tools that I worked with at some part during my testing phase.

*   GitHub (Repository)
*   GitHub Pages (Hosting)
*   Google Firebase (Hosting)
*   Amazon S3 Bucket (Hosting)
*   Amazon Route53 (Necessary for S3 Bucket Hosting)
*   Travis-CI (Hugo Builds/Deployment to Firebase Hosting)
*   GitLab (Repository, CI Build and Hosting)

Now, let's go over what I ended up with and why.

## Hugo

Because [Hugo is fast and easy to setup](https://gohugo.io/overview/installing/).

## GitLab

I was familiar with GitHub and knew about [GitLab](https://about.gitlab.com/), but had not had much experience with it yet. I am glad that I took the time to check it out. This one system allows me to host a private repo of my site content, build a Hugo site when there is a commit to the master branch and deploys the static content to GitLab pages. This is what I was looking for. See this page on the Hugo site about [Hosting on GitLab](https://gohugo.io/tutorials/hosting-on-gitlab/). A side benefit of GitLab is that I already had a Hugo site setup in my GitHub repository. It was really easy to import the project over from GitHub to GitLab and continue on from where I left off.

## GitLab CI

GitLab has a built in Continuous Integration/Deployment system, so no need for some external worker like Travis-CI, drone.io or Werker to do it for me. Just by creating a config file in the root of my site repo, I was able to trigger builds of my Hugo site and deployment to GitLab Pages. See my **[CI configuration file](https://gitlab.com/zivbk1/bryanklein.com/blob/master/.gitlab-ci.yml)** for how easy it is. NOTE: I added a bin directory to my repo, this makes it a little easier to control which version of Hugo I use locally and in the CI build, without having to edit the config file.  This also saves time on the build of the site, because the binary is already in the repo and I don't have to download and extract it before building the site.

## GitLab Pages

GitLab has a feature similar to GitHub for [hosting static site content](https://gitlab.com/help/pages/README.md), but I think it is a bit easier to use. It was also very easy to setup SSL for my custom domain which GitHub Pages cannot do now. In addition, paired with the Continuous Integration/Deployment feature, it would be just as easy to use any other static site generator and not just Hugo. I am a big fan of Hugo and having my site content updated in less than 30 seconds after a commit to the master branch is fantastic.

## Google Domains

I really like the simplicity of the [Google Domains](https://domains.google/#/) UI and I already had my domain registered with them, so this was the easiest part.

## Cloudflare

My [StartSSL Certificate was not trusted by browsers](https://security.googleblog.com/2016/10/distrusting-wosign-and-startcom.html), so I decided to try [Cloudflare](https://www.cloudflare.com/) as a CDN for my site and use their SSL system for content delivery.  There are two parts to this system. The first is the _[Origin Certificate](https://about.gitlab.com/2017/02/07/setting-up-gitlab-pages-with-cloudflare-certificates/)_, that you install on GitLab in your site certificate settings, this allows for a trusted connection between GitLab Pages content (Origin) and the CDN. The second part is the _[Edge Certificate](https://blog.cloudflare.com/dedicated-ssl-certificates/)_ that browsers use to securely connect with the CDN for your content.  Now I have end to end SSL in effect for my content.  
I was also able to take advantage of many of the Cloudflare features such as _Automatic HTTPS Rewrites_, _Auto Minify (for JS, CSS and HTML)_, _Caching_, and _Browser Cache Expiration_ for content. 

## Editing Content

### Forestry.io

I have been looking for an online editor for my Hugo content. There are many online Markdown editors, but few easily connect to a GitLab repository, [Forestry.io](https://forestry.io/) does this easily. The support for their service is amazing too, they quickly reply to chats and email. I look forward to working with them. Note: they also offer a build/deploy feature, but for my setup I don't need that. When the files/changes are pushed to my GitLab repository from Forestry.io, the build and deploy happens on GitLab and not on the Forestry side.

Update: Forestry.io just received a large [first round of funding](http://betakit.com/forestry-io-closes-first-seed-round-in-canada-with-angellist-syndicate/), which should help in development and growing their services.

### Mobile

I have an Android phone, and I have found the combo of [Pocket Git](https://play.google.com/store/apps/details?id=com.aor.pocketgit) and [DroidEdit Pro](https://play.google.com/store/apps/details?id=com.aor.droidedit.pro) to be a powerful one. I can open a file from Pocket Git in DroidEdit, make some changes and save it. Mark the edited files for commit and then Push the changes to my GitLab repo. As soon as the commit hits the repo, the build is triggered and within 20 seconds my site is updated. I am currently looking for a nice Markdown editor for my Android Phone/Tablet, that will make editing with real-time preview of the Markdown easy.  

I am hoping that Forestry.io will have a great mobile experience as they continue to develop their UI.  I have also discussed with Göran Svensson his development of [Appernetic](http://appernetic.io/) and his plans for a nice mobile app for content editing.  He is also very nice to communicate with and I look forward to the continued development of his services.

### Desktop

I use [MarkdownPad2](http://markdownpad.com/) and [Sublime Text 3](https://www.sublimetext.com/3) for local edits on my desktop. There are many nice editors out there with Markdown preview, so find what you like.  I connect to my GitLab repository with [SourceTree](https://www.sourcetreeapp.com/).

## Summary

To wrap this up, here is a basic list of steps in order that you would do to make a Hugo site on GitLab from scratch.

### Repository, Deployment and Hosting Related

1.  [Create an account](https://gitlab.com/users/sign_in) and [project for your site](https://docs.gitlab.com/ee/gitlab-basics/create-project.html) on GitLab
2.  Use [SourceTree](https://www.sourcetreeapp.com/) to clone your site to your local computer.
3.  [Setup Hugo on your local computer](https://gohugo.io/overview/installing/).
4.  [Initialize a new site with Hugo](https://gohugo.io/commands/hugo_new_site/) in the GitLab project folder you cloned (Will need the --force flag.)
5.  Work locally to [setup the Hugo site](https://gohugo.io/overview/quickstart/) as you want it with some basic content and a theme.
6.  Commit and then Push your site to GitLab, with the **[CI file as I have linked to above](https://gitlab.com/zivbk1/bryanklein.com/blob/master/.gitlab-ci.yml)** in your Hugo root folder.
7.  This should trigger the CI build and deployment to GitLab pages with your site to https://[username].gitlab.io/[projectname]

### Domain Related

1.  Register your custom domain with Google Domains
2.  Sign up for Cloudflare and get your Origin Certificate
3.  Setup your Custom domain with your Cloudflare Certificates and Private Key in GitLab Pages settings.
4.  Setup your Edge Certificates with Cloudflare for end-to-end encryption.

That's about it, please ask any questions or give suggestions for edits below.