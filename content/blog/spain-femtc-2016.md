+++
date = "2016-11-14T12:45:53-08:00"
draft = false
categories = ["Personal"]
title = "Spain, FEMTC 2016"
featured="towardMalaga.jpg"
featuredpath="/forestryio/images/"
featuredalt="Pano from our Hotel Room."
+++
Off to Spain for the Fire and Evacuation Modeling Technical Conference.
I've really been looking forward to this event and to be able to bring Carrie along with me.  Off we go!