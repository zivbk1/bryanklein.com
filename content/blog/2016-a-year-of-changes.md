+++
author = "Bryan Klein"
categories = ["Personal"]
date = "2016-11-08T17:46:02-08:00"
description = "It was the best of times, it was the worst of times..."
draft = false
featured = "/forestryio/images/change-sign-1.jpg"
featuredalt = "So many changes."
featuredpath = ""
linktitle = "2016 - A Year of Changes"
title = "2016 - A Year of Changes"
+++

Wow, what an amazing year. Some of the best and worst things to have happen in life happened this year. In rough chronological order.

<!--more-->

*   Dad became really sick and required hospitalization.
*   Marriage to Carrie!! :) An awesome wife and kids, nice weather, family and friends together, It was excellent!
*   Mom diagnosed with breast cancer.
*   Trip to Vegas with Carrie for Honeymoon. :)
*   Olivia's basketball team goes to State!
*   While at Olivia's State Tourney, come down with really bad case of the Flu that took me out for almost 2 weeks.
*   Mom surgery for cancer, starts chemo.
*   Create Yearbooks for Halle's kindergarten school for PTA.
*   End of house rental lease, start looking for a new one.
*   Find homes, make offers, close a deal.
*   Pack up and move out and clean up rental house.
*   Clean up, repair and update new house.
*   Move everything over and unpack into new house.
*   Carrie starts a new job 3 days a week.
*   Eli and Olivia move out of the house and away to college around the same time we are moving to new house.
*   Max starts pre-school and Halle starts 1st grade.
*   SFPE conference in Denver
*   FEMTC 2016 event planning and management.
*   Prep for Trip to Singapore then deliver 5 days of training there.
*   More FEMTC 2016 prep including coordination of 38 presenters for the event and over 100 in attendance.
*   2016 US Elections, Trump wins. O..o
*   Next week to Spain with Carrie for hosting the FEMTC event.
*   Excellent time in Spain with Carrie and friends.

Whew!!! And there is still a bit more left in the year. Fortunately, most of the really difficult stuff that we planned for is behind us now.

I wonder what 2017 will bring. :)