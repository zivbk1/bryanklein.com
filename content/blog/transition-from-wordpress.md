+++
author = "Bryan Klein"
categories = ["Professional"]
date = "2016-11-08T01:18:53-08:00"
description = "Leaving Wordpress behind after a TOS violation and spammer hack."
draft = false
featured = "/forestryio/images/hugo-engine.jpg"
featuredalt = ""
featuredpath = ""
linktitle = "Leaving Wordpress behind after a TOS violation and spammer hack."
title = "Transition from Wordpress"
+++

The subject of the email read, "TOS Violation: Spamvertising and hosting a redirect to a potentially fraudulent pharmacy site.", how could this be? I had the latest version of Wordpress installed, plug-ins were up to date. Somehow a creepy bot or person figured out a way in and started using my domain for their own benefit.

<!--more-->

I've been contemplating a departure from Wordpress both for my website here and for our company site. This email from my hosting company was the motive force to export my old content, delete the Wordpress installation and make a switch to a static site. I have tried out quite a few Static Site Generators and my favorite for various reasons was Hugo. So after a quick setup I have this site up and running with some demo content and some peace of mind.

Now that I have this working, it will be interesting to see how I can work with hugo to do my bidding. As I sit here typing into a plain text editor, I wonder what tools and techniques I will discover to make posting regular content to my site easy and enjoyable.

It's too late now to bother with it more tonight, but I am happy that the majority of the transition is complete and I look forward to the future with Hugo.