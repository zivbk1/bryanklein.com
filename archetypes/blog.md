+++
title = "{{ replace .Name "-" " " | title }}"
author = "Bryan Klein"
categories = ["Professional", "Personal"]
date = {{ .Date }}
description = ""
linktitle = ""
featured = ""
featuredpath = ""
featuredalt = ""
draft = true
+++
